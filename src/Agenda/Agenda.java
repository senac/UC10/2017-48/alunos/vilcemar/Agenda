
package agenda;

import br.com.senac.model.Contato;
import br.com.senac.model.view.JFramePrincipal;
import java.util.ArrayList;
import java.util.List;





public class Agenda {
    
         private static List<Contato> listaDeContatos = new ArrayList<>();
   
    public static void adicionar(Contato contato){
        listaDeContatos.add(contato);
    }
    
    public static void remover(Contato contato){
        listaDeContatos.remove(contato);
    }
    
    public static void remover(int indice){
        listaDeContatos.remove(indice);
    }
    
    public static int getQuantidadeContatos(){
        return listaDeContatos.size();
    }
    
    public static void main(String[] args) {
        JFramePrincipal janela = new JFramePrincipal();
        janela.setVisible(true);
             
        
    }
}
