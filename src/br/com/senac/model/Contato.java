
package br.com.senac.model;


public class Contato {
    private String nome;
    private String telefone;
    private String email;
    private String tipoContato;

    public Contato() {
    }

    public Contato(String nome, String telefone, String email, String tipoContato) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.tipoContato = tipoContato;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTipoContato(String tipoContato) {
        this.tipoContato = tipoContato;
    }

    
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the tipoContato
     */
    public String getTipoContato() {
        return tipoContato;
    }
    
    
    
}
